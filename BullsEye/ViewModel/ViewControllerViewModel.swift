//
//  ViewControllerViewModel.swift
//  BullsEye
//
//  Created by rjs on 2/7/22.
//

import Foundation
struct ViewControllerViewModel {
    var score = 0 {
        didSet {
            scoreLabelString = "Score: \(score)"
        }
    }
    var sliderValue: Int = 50
    var round = 1 {
        didSet {
            roundLabelString = "Round: \(round)"
        }
    }
    
    var targetValue: Int = 0 {
        didSet {
            primaryLabelString = "Put the Bull's Eye as close as you can to: \(targetValue)"
        }
    }
    
    var scoreTitle = ""
    var scoreMessage = ""
    
    var primaryLabelString: String = ""
    var roundLabelString: String = ""
    var scoreLabelString = ""
    
    init() {
        targetValue = generateRandomTarget()
        primaryLabelString = "Put the Bull's Eye as close as you can to: \(targetValue)"
        roundLabelString = "Round: \(round)"
        scoreLabelString = "Score: \(score)"
    }
    
    private func generateRandomTarget() -> Int {
        return Int.random(in: 1...100)
    }
    
    mutating func startNewRound() {
        targetValue = generateRandomTarget()
        sliderValue = 50
        round += 1
    }
    
    mutating func startNewGame() {
            score = 0
        targetValue = generateRandomTarget()
        sliderValue = 50
        round = 1
    }
    
    mutating func scoreUserAttempt() {
        var points = self.calculatePoints(targetValue: targetValue, checkValue: sliderValue)
        let difference = self.calculateDifference(targetValue: targetValue, checkValue: sliderValue)
        points += self.calculateBonusScore(difference)
        
        // add these lines
        if difference == 0 {
            scoreTitle = "Perfect!"
        } else if difference < 5 {
            scoreTitle = "You almost had it!"
        } else if difference < 10 {
            scoreTitle = "Pretty good!"
        } else {
            scoreTitle = "Not even close..."
        }
        
        score += points
        scoreMessage = "You scored \(points) points"
    }
    
    private func calculatePoints(targetValue: Int, checkValue: Int) -> Int {
        let difference = self.calculateDifference(targetValue: targetValue, checkValue: checkValue)
        let points = 100 - difference
        return points
    }
    
    private func calculateDifference(targetValue: Int, checkValue: Int) -> Int {
        let difference = abs(targetValue - checkValue)
        return difference
    }
    
    private func calculateBonusScore(_ difference: Int) -> Int {
        var bonus = 0
        if difference == 0 {
            bonus = 250
        } else if difference < 5 {
            bonus = 100
        } else if difference < 10 {
            bonus = 25
        } else {
            bonus = -25
        }
        return bonus
    }
}
