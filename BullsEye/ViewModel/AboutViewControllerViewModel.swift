//
//  AboutViewControllerViewModel.swift
//  BullsEye
//
//  Created by rjs on 2/9/22.
//

import Foundation

struct AboutViewControllerViewModel {
    let rulesTextString = "*** Bull's Eye ***\n\n\nWelcome to the awesome game of Bull's Eye where you can win points and fame by dragging a slider.\n\n\nYour goal is to place the slider as close as possible to the target value. The closer you are, the more points you score. Enjoy!"
}
