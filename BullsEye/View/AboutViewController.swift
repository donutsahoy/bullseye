//
//  AboutViewController.swift
//  BullsEye
//
//  Created by rjs on 2/9/22.
//

import UIKit
import WebKit

class AboutViewController: UIViewController {
    
    //MARK: - Properties
    private let viewModel = AboutViewControllerViewModel()
    
    private let backgroundImage: UIImageView = {
        let iv = UIImageView(image: UIImage(imageLiteralResourceName: "Background"))
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
//    private lazy var textView: UITextView = {
//        let tv = UITextView()
//        tv.isEditable = false
//        tv.text = viewModel.rulesTextString
//        tv.textColor = .white
//        tv.font = UIFont(name: "Arial Rounded MT Bold", size: 20)
//        tv.backgroundColor = .clear
//        return tv
//    }()
    
    private let webView: WKWebView = {
        let wk = WKWebView()
        if let url = Bundle.main.url(
            forResource: "BullsEye", withExtension: "html") {
            let request = URLRequest(url: url)
            wk.load(request)
        }
        return wk
    }()
    
    private let closeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setBackgroundImage(UIImage(imageLiteralResourceName: "Button-Normal"), for: .normal)
        button.setTitle("Close", for: .normal)
        button.addTarget(self, action: #selector(onClosePressed), for: .touchUpInside)
        button.titleLabel?.textColor = UIColor(red: 96, green: 30, blue: 0, alpha: 1)
        button.titleLabel?.shadowColor = .white
        button.titleLabel?.layer.shadowOpacity = 0.5
        button.titleLabel?.shadowOffset = CGSize(width: 0, height: 1)
        button.titleLabel?.font = UIFont(name: "Arial Rounded MT Bold", size: 20)
        
        return button
    }()
    
    //MARK: - Actions
    @objc func onClosePressed() {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    //MARK: - Helpers
    
    func configure() {
        view.addSubview(backgroundImage)
        backgroundImage.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor)
   
        view.addSubview(closeButton)
        closeButton.anchor(bottom: view.bottomAnchor, paddingBottom: 32)
        closeButton.centerX(inView: view)
        
        view.addSubview(webView)
        webView.anchor(top: view.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: closeButton.topAnchor, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 8,  paddingBottom: 8)
        webView.centerX(inView: view)
        

    }
}
