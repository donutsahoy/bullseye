//
//  ViewController.swift
//  BullsEye
//
//  Created by rjs on 2/6/22.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - Properties
    var viewModel = ViewControllerViewModel()
    
    private let backgroundImage: UIImageView = {
        let iv = UIImageView(image: UIImage(imageLiteralResourceName: "Background"))
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    private let primaryLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.shadowColor = .black
        label.layer.shadowOpacity = 0.5
        label.shadowOffset = CGSize(width: 0, height: 1)
        label.font = UIFont(name: "Arial Rounded MT Bold", size: 16)
        return label
    }()
    
    private let startingNumberLabel: UILabel = {
        let label = UILabel()
        label.text = "1"
        label.textColor = .white
        label.shadowColor = .black
        label.layer.shadowOpacity = 0.5
        label.shadowOffset = CGSize(width: 0, height: 1)
        label.font = UIFont(name: "Arial Rounded MT Bold", size: 16)
        return label
    }()
    
    private lazy var slider: UISlider = {
        let sl = UISlider()
        sl.minimumValue = 1
        sl.maximumValue = 100
        sl.value = Float(viewModel.sliderValue)
        sl.addTarget(self, action: #selector(sliderMoved(_:)), for: .valueChanged)
        
        let thumbImageNormal = UIImage(named: "SliderThumb-Normal")!
        sl.setThumbImage(thumbImageNormal, for: .normal)
        
        let thumbImageHighlighted = UIImage(named: "SliderThumb-Highlighted")!
        sl.setThumbImage(thumbImageHighlighted, for: .highlighted)
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        let trackLeftImage = UIImage(named: "SliderTrackLeft")!
        let trackLeftResizable = trackLeftImage.resizableImage(withCapInsets: insets)
        sl.setMinimumTrackImage(trackLeftResizable, for: .normal)
        
        let trackRightImage = UIImage(named: "SliderTrackRight")!
        let trackRightResizable = trackRightImage.resizableImage(withCapInsets: insets)
        sl.setMaximumTrackImage(trackRightResizable, for: .normal)
        return sl
    }()
    
    private let endingNumberLabel: UILabel = {
        let label = UILabel()
        label.text = "100"
        label.textColor = .white
        label.shadowColor = .black
        label.layer.shadowOpacity = 0.5
        label.shadowOffset = CGSize(width: 0, height: 1)
        label.font = UIFont(name: "Arial Rounded MT Bold", size: 16)
        return label
    }()
    
    private let hitMeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setBackgroundImage(UIImage(imageLiteralResourceName: "Button-Normal"), for: .normal)
        button.setTitle("Hit Me!", for: .normal)
        button.addTarget(self, action: #selector(showAlert), for: .touchUpInside)
        button.titleLabel?.textColor = UIColor(red: 96, green: 30, blue: 0, alpha: 1)
        button.titleLabel?.shadowColor = .white
        button.titleLabel?.layer.shadowOpacity = 0.5
        button.titleLabel?.shadowOffset = CGSize(width: 0, height: 1)
        button.titleLabel?.font = UIFont(name: "Arial Rounded MT Bold", size: 20)
        
        return button
    }()
    
    private let startOverButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(imageLiteralResourceName: "StartOverIcon"), for: .normal)
        button.setBackgroundImage(UIImage(imageLiteralResourceName: "SmallButton"), for: .normal)
        button.addTarget(self, action: #selector(startOverPressed), for: .touchUpInside)
        return button
    }()
    
    private let scoreLabel: UILabel = {
        let label = UILabel()
        label.text = "Score: 0"
        label.textColor = .white
        label.shadowColor = .black
        label.layer.shadowOpacity = 0.5
        label.shadowOffset = CGSize(width: 0, height: 1)
        label.font = UIFont(name: "Arial Rounded MT Bold", size: 16)
        return label
    }()
    
    private let roundLabel: UILabel = {
        let label = UILabel()
        label.text = "Round: 0"
        label.textColor = .white
        label.shadowColor = .black
        label.layer.shadowOpacity = 0.5
        label.shadowOffset = CGSize(width: 0, height: 1)
        label.font = UIFont(name: "Arial Rounded MT Bold", size: 16)
        return label
    }()
    
    private let infoButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(imageLiteralResourceName: "InfoButton"), for: .normal)
        button.setBackgroundImage(UIImage(imageLiteralResourceName: "SmallButton"), for: .normal)
        button.addTarget(self, action: #selector(infoButtonPressed), for: .touchUpInside)
        return button
    }()
    
    //MARK: - Actions
    @objc func infoButtonPressed() {
        let controller = AboutViewController()
        let nav = UINavigationController(rootViewController: controller)
        
        nav.modalPresentationStyle = .fullScreen
        nav.modalTransitionStyle = .flipHorizontal
        self.present(nav, animated: true, completion: nil)
    }
    
    @objc func showAlert() {
        viewModel.scoreUserAttempt()
        
        let alert = UIAlertController(title: viewModel.scoreTitle, message: viewModel.scoreMessage, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default) { _ in
            self.reset()
        }
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func sliderMoved(_ slider: UISlider) {
        viewModel.sliderValue = lroundf(slider.value)
    }
    
    @objc func startOverPressed() {
        viewModel.startNewGame()
        updateStringValues()
        
        let transition = CATransition()
        transition.type = CATransitionType.fade
        transition.duration = 1
        transition.timingFunction = CAMediaTimingFunction(
            name: CAMediaTimingFunctionName.easeOut)
        view.layer.add(transition, forKey: nil)
    }
    
    func reset() {
        viewModel.startNewRound()
        updateStringValues()
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func updateStringValues() {
        primaryLabel.text = viewModel.primaryLabelString
        roundLabel.text = viewModel.roundLabelString
        scoreLabel.text = viewModel.scoreLabelString
        slider.value = Float(viewModel.sliderValue)
    }
    
    func configure() {
        view.addSubview(backgroundImage)
        backgroundImage.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor)
        
        view.addSubview(hitMeButton)
        hitMeButton.anchor(width: 100, height: 37)
        hitMeButton.center(inView: view)
        
        let slideStack = UIStackView(arrangedSubviews: [startingNumberLabel, slider, endingNumberLabel])
        slideStack.axis = .horizontal
        slideStack.spacing = 6
        
        view.addSubview(slideStack)
        slideStack.centerX(inView: view)
        slideStack.anchor(bottom: hitMeButton.topAnchor, paddingBottom: 32, width: 300)
        
        view.addSubview(primaryLabel)
        primaryLabel.centerX(inView: view)
        primaryLabel.anchor(bottom: slideStack.topAnchor, paddingBottom: 32)
        
        let bottomStack = UIStackView(arrangedSubviews: [startOverButton, scoreLabel, roundLabel, infoButton])
        startOverButton.anchor(width: 32, height: 32)
        infoButton.anchor(width: 32, height: 32)
        bottomStack.axis = .horizontal
        bottomStack.spacing = 48
        view.addSubview(bottomStack)
        bottomStack.centerX(inView: view)
        bottomStack.anchor(top: hitMeButton.bottomAnchor, paddingTop: 32)
        
        updateStringValues()
    }
    
}

